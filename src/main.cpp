//////////////////////////////////////////////////////////////////

//  Designer name: Jared Diamond                                //

//  Project name: Pocket Logger                                 //

//  Program description:                                        //

//                                                              //

//////////////////////////////////////////////////////////////////

#include <Arduino.h>
#include <TFT_eSPI.h>
#include <SPI.h>
#include <mySD.h>

// Button defines
#define BUTTON1PIN 35 // bottom built in button of board
#define BUTTON2PIN 0  // top built in button of board
// Encoder defines and init
#define EBUTTON 21 // encoder button
#define ENCODEA 17 // A pin of encoder
#define ENCODEB 22 // B pin of encoder
// SD card reader defines and init
#define HSPI_MISO 27
#define HSPI_MOSI 26
#define HSPI_SCLK 25
#define HSPI_CS 33
Sd2Card card;
// TFT screen defines
TFT_eSPI tft = TFT_eSPI();
TFT_eSprite img = TFT_eSprite(&tft);

long encodeTotal = 0; // encoder total that is changed on encoder turn or press

// Debounce variables
unsigned long lastClick = 0; // The last time an encoder interrupt happened. Set to millis() on interrupt
int debounceDelay = 30;      // 30ms between re-polling

int incValue = 1; // holds value for incrementing/decrementing encoderTotal

void showScrn2(long eValue);
void showScrn1();
void debugSD();

void showScrn2(long eValue)
{

  char encodeString[60] = "";           // initializing encoder total string
  sprintf(encodeString, "%ld", eValue); // converting encoder total (long int) to string

  img.fillSprite(TFT_BLACK); // refreshing screen by clearing old output

  img.drawCentreString("Encoder Total:", 120, 25, 4);
  img.drawCentreString(encodeString, 120, 67, 6);
  img.pushSprite(0, 0); // sent to display
  return;
}
// Encoder's dial has been turned
void encoderTurned()
{

  // If it has been greater than 30ms since last interrupt, run interrupt code again
  // This helps mitigate repeated interrupt calls from mechanical parts "bouncing"
  if ((millis() - lastClick) > debounceDelay)
  {

    int encodeA = digitalRead(ENCODEA);
    int encodeB = digitalRead(ENCODEB);

    if (encodeA == 0 && encodeB == 0)
      encodeTotal += incValue; // Encoder has been turned clockwise. Add one to number being added to dataset
    else if (encodeA == 0 && encodeB == 1)
      encodeTotal -= incValue; // Encoder has been turned counter-clockwise. Subtract one from number being added to dataset

    showScrn2(encodeTotal);
  }

  lastClick = millis();
  return;
}
// Encoder's button has been pressed
void encoderPressed()
{

  if ((millis() - lastClick) > debounceDelay)
  {
    encodeTotal += 100; // Add 100 to the number being added to dataset
    showScrn2(encodeTotal);
  }
  lastClick = millis();
  return;
}
void setup()
{

  Serial.begin(115200);

  // Encoder Setup
  pinMode(ENCODEA, INPUT_PULLUP); // A input of encoder defined
  pinMode(ENCODEB, INPUT_PULLUP); // B input of encoder defined
  pinMode(EBUTTON, INPUT_PULLUP); // Button on encoder defined

  // ENCODER A INTERRUPT ARMED
  attachInterrupt(ENCODEA, encoderTurned, RISING);
  // ENCODER BUTTON INTERRUPT ARMED
  attachInterrupt(EBUTTON, encoderPressed, RISING);
  // TFT screen setup
  tft.begin();
  tft.setRotation(1);         //Landscape
  img.createSprite(240, 135); // size of screen defined
  img.setTextColor(TFT_WHITE, TFT_WHITE);
  img.fillSprite(TFT_BLACK);

  // SD card reader setup
  pinMode(HSPI_CS, OUTPUT); //HSPI SS
  //debugSD();
}

void loop()
{
  delay(1000);
}
// Alerts the user if there are problems reading the SD card
// void debugSD()
// {
//   while (!card.init(SPI_HALF_SPEED, HSPI_CS, HSPI_MOSI, HSPI_MISO, HSPI_SCLK))
//   {
//     Serial.println("initialization failed. Things to check:");
//     Serial.println("* is a card is inserted?");
//     Serial.println("* Is your wiring correct?");
//     Serial.println("* did you change the chipSelect pin to match your shield or module?");
//   }

//   // print the type of card
//   Serial.print("\nCard type: ");
//   switch (card.type())
//   {
//   case SD_CARD_TYPE_SD1:
//     Serial.println("SD1");
//     break;
//   case SD_CARD_TYPE_SD2:
//     Serial.println("SD2");
//     break;
//   case SD_CARD_TYPE_SDHC:
//     Serial.println("SDHC");
//     break;
//   default:
//     Serial.println("Unknown");
//   }
//   return;
// }
